
Feature: DemoWebShop Automation

    Scenario: Launch DemoWebShop and Login Functionality
    Given User Should be navigate to demowebshop WebApplictaion
    When The user click on the login button
    And  User should enter a valid EmailId and Password
    Then click on the login button

    Scenario: Select books and add to cart
    Given Click the Books 
    When User click on category and Sort by to price high to low
    Then User add two book in add to cart
  
  
    Scenario: Select Electronics and add cellphone to cart
    Given Click the electronic 
    And   The user click on category and select the cellphone
    When  User add any Phone product in add cart
    Then  The display count of all items present in shopping cart
    
    Scenario: Select giftcard and print the details
    Given The User clicking on the Giftcard 
    When  User click on giftcard displayed per page and select the value 4
    Then  User select a gift card and get its details
    
    Scenario: Logout from DemoWebShop
    Given User clicks on logout buuton
    When User verifies after logout login is displayed in home page
    Then Close the browser