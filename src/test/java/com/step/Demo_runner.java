package com.step;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
@CucumberOptions(features="src/test/resources/Feature/DemoWebShop.feature",
glue= {"com.step"})
public class Demo_runner extends AbstractTestNGCucumberTests{
}