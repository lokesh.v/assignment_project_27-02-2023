package com.step;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import com.generics.Driverutils;
import com.page.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Steps {

	public static WebDriver driver;
	LoginPage lp= new LoginPage();
	BooksPage bp=new BooksPage();
	ElectronicsPage ep=new ElectronicsPage();
	GiftCardPage gp=new GiftCardPage();
	LogOutPage lo=new LogOutPage();
	
	@Given("User Should be navigate to demowebshop WebApplictaion")
	public void user_should_be_navigate_to_demowebshop_web_applictaion() {
		 Driverutils.getdriver().get("https://demowebshop.tricentis.com/login");  
		 
	}

	@When("The user click on the login button")
	public void the_user_click_on_the_login_button() {
	    lp.login();
	   
	}

	@When("User should enter a valid EmailId and Password")
	public void user_should_enter_a_valid_email_id_and_password() {
		 lp.email();
		 lp.password();
		  
	}

	@Then("click on the login button")
	public void click_on_the_login_button() {
		  lp.loginbut();
	}

	@Given("Click the Books")
	public void click_the_Books() {
	  bp.books();
	}

	@When("User click on category and Sort by to price high to low")
	public void user_click_on_category_and_sort_by_to_price_high_to_low() {
	   bp.sortBy();
	}

	@Then("User add two book in add to cart")
	public void user_add_two_book_in_add_to_cart() {
	   bp.scienceBook();
	   bp.computing();
	}

	@Given("Click the electronic")
	public void click_the_electronic() {
	  ep.electronics();
	}

	@Given("The user click on category and select the cellphone")
	public void the_user_click_on_category_and_select_the_cellphone() {
	   ep.phone();
	}

	@When("User add any Phone product in add cart")
	public void user_add_any_phone_product_in_add_cart() {
	   ep.addPhone();
	}

	@Then("The display count of all items present in shopping cart")
	public void the_display_count_of_all_items_present_in_shopping_cart() {
	   ep.Cart();
	}

	@Given("The User clicking on the Giftcard")
	public void the_user_clicking_on_the_giftcard() {
	   gp.giftCard();
	}

	@When("User click on giftcard displayed per page and select the value {int}")
	public void user_click_on_giftcard_displayed_per_page_and_select_the_value(Integer int1) {
	  gp.cardDisplayed_Per_Page();
	}

	@Then("User select a gift card and get its details")
	public void user_select_a_gift_card_and_get_its_details() {
	 gp.DetailsOfCard();
	}

	@Given("User clicks on logout buuton")
	public void user_clicks_on_logout_buuton() {
	   lo.logOut();
	}

	@When("User verifies after logout login is displayed in home page")
	public void user_verifies_after_logout_login_is_displayed_in_home_page() {
       lo.logInIsPresent();
	}

	@Then("Close the browser")
	public void close_the_browser() {
	  lo.closeBrowser();
	}
		
}
	
