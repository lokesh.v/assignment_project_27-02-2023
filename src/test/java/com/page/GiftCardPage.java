package com.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.base.BaseClass;

public class GiftCardPage extends BaseClass{

	@FindBy(partialLinkText = "Gift Cards")
	public WebElement GiftCard;
	@FindBy(id = "products-pagesize")
	public WebElement CardperPage;
	@FindBy(xpath = "//div[@data-productid='1']//div[@class='details']")
	public WebElement Details;
	
	
	
	public void giftCard(){
		GiftCard.click();
	}
	public void cardDisplayed_Per_Page() {
		Select s= new Select(CardperPage);
		s.selectByVisibleText("4");
	}
	public void DetailsOfCard() {
		String det= Details.getText();
		System.out.println("Details of first card :"+det);
	}
	
}
