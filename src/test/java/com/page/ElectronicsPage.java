package com.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.base.BaseClass;

public class ElectronicsPage extends BaseClass{

	@FindBy(partialLinkText = "Electronics")
	public WebElement Electronics;
	@FindBy(partialLinkText = "Cell phone")
	public WebElement Phone;
	@FindBy(xpath = "(//input[@value='Add to cart'])[1]")
	public WebElement AddtoCart;
	@FindBy(xpath = "//span[@class='cart-qty']")
	public WebElement Item_in_Cart;
	
	
	
	
	
	
public void electronics() {
	Electronics.click();
}	
public void phone() {
	Phone.click();
}
public void addPhone() {
	AddtoCart.click();
}
public void Cart() {
	String count=Item_in_Cart.getText();
	System.out.println("Total no of items in cart is :"+count);
}
}
