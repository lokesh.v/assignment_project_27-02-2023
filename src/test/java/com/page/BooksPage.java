package com.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.base.BaseClass;

public class BooksPage extends BaseClass {

	@FindBy(partialLinkText = "Books")
	public WebElement Books;
	@FindBy(id = "products-orderby")
	public WebElement Sort;
	@FindBy(xpath = "(//input[@value='Add to cart'])[1]")
	public WebElement scienceBook;
	@FindBy(xpath = "(//input[@value='Add to cart'])[2]")
	public WebElement Computing;
	
	
	
public void books() {
	Books.click();
}	
public void sortBy() {
	Select s=new Select(Sort);
	s.selectByVisibleText("Price: High to Low");
}	
public void scienceBook() {
	scienceBook.click();
}
public void computing() {
	Computing.click();
}

}
