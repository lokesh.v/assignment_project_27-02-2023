package com.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.base.BaseClass;

public class LogOutPage extends BaseClass {

	@FindBy(linkText = "Log out")
	public WebElement LogOut;
	@FindBy(linkText = "Log in")
	public WebElement LogIn;
	
	
	
	
	
	
	
	public void logOut() {
		LogOut.click();
	}
	public void logInIsPresent() {
		String ActualText=LogIn.getText();
		String ExceptedText="Log in";
		Assert.assertEquals(ExceptedText, ActualText);
		System.out.println("LogIn is present in the display");
		
	}
	public void closeBrowser() {
		driver.close();
	}
}
