package com.testng;

import java.time.Duration;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.base.BaseClass;
import com.page.*;

public class TestNg extends BaseClass {
	
	

	@BeforeClass
	public void launch() {
		driver.get("https://demowebshop.tricentis.com/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(8));
	}
  @Test(priority = 1)
  public void loginPage () {
	  LoginPage lp= PageFactory.initElements(driver, LoginPage.class);
	  lp.login();
	  lp.email();
	  lp.password();
	  lp.loginbut();
	
  }
  @Test(priority = 2)
  public void bookPage() {
	  BooksPage bp= PageFactory.initElements(driver, BooksPage.class);
	  bp.books();
	  bp.sortBy();
	  bp.scienceBook();
	  bp.computing();
	  
  }
  @Test (priority = 3)
  public void electronicsPage() {
	  ElectronicsPage ep= PageFactory.initElements(driver, ElectronicsPage.class);
	  ep.electronics();
	  ep.phone();
	  ep.addPhone();
	  ep.Cart();
  }
  @Test(priority = 4)
  public void giftcardPage() {
	  GiftCardPage gp= PageFactory.initElements(driver, GiftCardPage.class);
	  gp.giftCard();
	  gp.cardDisplayed_Per_Page();
	  gp.DetailsOfCard();
  }
  @Test(priority = 5)
  public void logOut() {
	  LogOutPage lo= PageFactory.initElements(driver, LogOutPage.class);
	  lo.logOut();
	  lo.logInIsPresent();
	  lo.closeBrowser();
  }
}
