package com.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.generics.Driverutils;

public class BaseClass {

	public static WebDriver driver;
    
    public BaseClass() {
        driver=Driverutils.getdriver();
        PageFactory.initElements(driver, this);
    }

}
