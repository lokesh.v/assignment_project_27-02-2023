package com.generics;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

	public class Driverutils {
		static WebDriver driver;

			public static void createdriver() {
				driver = new ChromeDriver();
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7));
			}
			public static WebDriver getdriver() {
				if (driver == null) {
					createdriver();
				}
				return driver;
			
	}}


